# frozen_string_literal: true

require 'json'
require 'sinatra'
require 'socket'
require 'yaml'

def check_key(key)
  config = YAML.load_file 'config.yaml'
  halt 401 if key != config['key']
end

def check_webhook_key(key)
  config = YAML.load_file 'config.yaml'
  halt 401 unless config['webhook_keys'].include? key
end

def forward_message(message)
  sock = TCPSocket.open 'localhost', 2000
  sock.puts message
rescue Errno::ECONNREFUSED
  halt 500
end

get '/' do
  'Nanobot API ready.'
end

post '/v1/message' do
  check_key params[:key]

  return 'Parameter dest is missing' unless params.key? 'dest'
  return 'Parameter message is missing' unless params.key? 'message'

  forward_message({ dest: params[:dest], message: params[:message] }.to_json)

  'Message accepted'
end

post '/v1/webhook/:webhook_key' do
  check_webhook_key params[:webhook_key]

  halt 400 unless params.key? 'payload'

  payload = JSON.parse(params[:payload])
  halt 400 unless payload.key? 'text'

  payload['channel']  = '#shells' unless payload.key? 'channel'
  payload['username'] = 'webhook' unless payload.key? 'username'

  forward_message({
    dest: payload['channel'],
    message: "#{payload['username']}: #{payload['text']}"
  }.to_json)
  'OK'
end

post '/v2/webhook/:webhook_key' do
  check_webhook_key params[:webhook_key]

  payload = JSON.parse(request.body.read)
  halt 400 unless payload.key? 'text'
  
  payload['channel']  = '#shells' unless payload.key? 'channel'
  payload['username'] = 'webhook' unless payload.key? 'username'

  forward_message({
    dest: payload['channel'],
    message: "#{payload['username']}: #{payload['text']}"
  }.to_json)
  'OK'
end
