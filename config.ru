# frozen_string_literal: true

require File.expand_path 'nanoapi_app.rb', __dir__

run Sinatra::Application
